import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Store from './Components/Common/Utils/Redux/Store';

// PathsInterface
import { PathsLabel } from "./Components/Common/Utils/Paths";
import Menu from '../src/Components/Common/Menu/Menu';

// Styles
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Components
import Login from './Components/Common/User/Login';
import Register from './Components/Common/User/RegisterStudent';
import Calls from './Components/Calls/CallList';
import CallDetails from './Components/Calls/CallDetails';
import DataItems from './Components/DataItems/DataItemsList';
import DataItemDetails from './Components/DataItems/DataItemDetails';
import PanelDetails from './Components/Panels/PanelDetails';
import Applications from './Components/Applications/ApplicationList';
import ApplicationDetails from './Components/Applications/ApplicationDetails';
import Reviewers from './Components/Reviewers/ReviewersList';
import ReviewerDetails from './Components/Reviewers/ReviewerDetails';
import Students from './Components/Students/StudentsList';
import Institutions from './Components/Institutions/InstitutionList';
import InstitutionDetails from './Components/Institutions/InstitutionDetails';
import DataInCalls from './Components/DataItems/DataInCalls';
import ReviewDetails from './Components/Review/ReviewDetails';



export default class App extends Component {

      render() {
            return (
                  <HashRouter>
                        <Provider store={Store}>
                              <Menu />
                              <Switch>
                                    <Route exact path={PathsLabel.Login} component={Login} />
                                    <Route exact path={PathsLabel.Register} component={Register} />
                                    <Route exact path={PathsLabel.Calls} component={Calls} />
                                    <Route exact path={PathsLabel.Calls + PathsLabel.Details} component={CallDetails} />
                                    <Route exact path={PathsLabel.DataItems} component={DataItems} />
                                    <Route exact path={PathsLabel.DataItems + PathsLabel.Details} component={DataItemDetails} />
                                    <Route exact path={PathsLabel.Panels + PathsLabel.Details} component={PanelDetails} />
                                    <Route exact path={PathsLabel.Applications} component={Applications} />
                                    <Route exact path={PathsLabel.Applications + PathsLabel.Details} component={ApplicationDetails} />
                                    <Route exact path={PathsLabel.Reviewer} component={Reviewers} />
                                    <Route exact path={PathsLabel.Reviewer + PathsLabel.Details} component={ReviewerDetails} />
                                    <Route exact path={PathsLabel.Students} component={Students} />
                                    <Route exact path={PathsLabel.Institutions} component={Institutions} />
                                    <Route exact path={PathsLabel.Institutions + PathsLabel.Details} component={InstitutionDetails} />
                                    <Route exact path={PathsLabel.Reviews + PathsLabel.Details} component={ReviewDetails} />
                                    <Route exact path={PathsLabel.Calls + PathsLabel.DataItems} component={DataInCalls} />


                                    {/**<Route exact path = '/' component = {Home}/>*/}
                              </Switch>
                        </Provider>
                  </HashRouter>
            );
      }
}
