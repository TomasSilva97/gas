import React, { Component } from 'react';
import axios from 'axios';
import { IoTrashOutline } from "react-icons/io5";
import { Table, Button, Row, Col, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { ReviewerInPanelStruct, ReviewerStruct } from '../Common/Utils/StructTypes';
import { PathsLabel, ServicePathsLabel, SubServicesLable } from '../Common/Utils/Paths';

class PanelDetails extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            id: this.props.id,
            title: this.props.title,
            reviewersInPanel: [], // reviewersInPanel
            reviewers: [] //populates the combo box
        }
        this.addReviewerInPanel = this.addReviewerInPanel.bind(this);
        this.savePanel = this.savePanel.bind(this);
        this.reviewerDetails = this.reviewerDetails.bind(this);
        this.deleteReviewerInPanel = this.deleteReviewerInPanel.bind(this);
        this.getReviewerNameById = this.getReviewerNameById.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        // Get reviewers that exist
        axios.get(ServicePathsLabel.Reviewers)
            .then(response => {
                this.setState({ reviewers: response.data });
            })
            .catch(error => {
                console.log("Error: " + error);
            });

        // gets reviewersInPanelInPanel
        axios.get(ServicePathsLabel.Calls + SubServicesLable.ReviewersInPanels + this.props.id)
            .then(response => {
                var reviewersInPanel: [ReviewerInPanelStruct] = response.data;
                reviewersInPanel.forEach(reviewerInPanel => reviewerInPanel.isLocal = false);
                this.setState({ reviewersInPanel: reviewersInPanel });
                console.log(response.data)
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    addReviewerInPanel() {
        var reviewerInPanel = {
            id: 0,
            panelId: this.props.id,
            reviewerId: this.state.reviewers[0].id,
            isChair: false,
            isLocal: true
        }
        var reviewersInPanel = this.state.reviewersInPanel;
        reviewersInPanel.push(reviewerInPanel);
        this.setState({ reviewersInPanel: reviewersInPanel })
    }

    savePanel() {
        var reviewersInPanelArray: Array<ReviewerInPanelStruct> = [];

        this.state.reviewersInPanel.forEach((el: ReviewerInPanelStruct) => {
            var newReviewer = { id: el.id, panelId: el.panelId, reviewerId: el.reviewerId, isChair: el.isChair };
            reviewersInPanelArray.push(newReviewer);
        })
        console.log(reviewersInPanelArray)

        axios.put(ServicePathsLabel.Calls + SubServicesLable.Panels + this.props.id, reviewersInPanelArray)
            .then(_response => {
                window.location.hash = PathsLabel.Calls;
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    reviewerDetails = (id: number) => (event: React.MouseEvent) => {
        if (id === -1) {
            this.props.cleanReviewerId(); // removes from the store
        } else {
            this.props.addReviewerId(id); // adds id to store
        }
        window.location.hash = PathsLabel.Reviewer + PathsLabel.Details;
        event.preventDefault();
    }

    deleteReviewerInPanel = (id: number, index: number, isLocal: boolean) => async (event: any) => {
        if (isLocal) {
            var newReviewersInPanel = this.state.reviewersInPanel;
            newReviewersInPanel.splice(index, 1);

            this.setState({ reviewersInPanel: newReviewersInPanel });
        } else {
            await axios.delete(ServicePathsLabel.Calls + SubServicesLable.ReviewersInPanels + id)
                .then(response => {
                    var newReviewersInPanel = this.state.reviewersInPanel.filter((el: ReviewerInPanelStruct) => el.id !== id);
                    this.setState({ reviewersInPanel: newReviewersInPanel });
                })
                .catch(error => {
                    console.log("Error: " + error);
                });
        }
        event.preventDefault();
    }

    getReviewerNameById(id: number) {
        console.log(id)
        var reviewer = this.state.reviewers.find((el: ReviewerStruct) => el.id === id);
        var name = "";
        if (reviewer) {
            name = reviewer.name;

            return (
                <td>
                    {name}
                </td>
            );
        }
    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        var index = e.target.options.selectedIndex;

        var reviewersInPanel: [ReviewerInPanelStruct] = this.state.reviewersInPanel;
        reviewersInPanel[index].reviewerId = parseInt(value, 10);

        this.setState({ ...this.state, reviewersInPanel: reviewersInPanel });
    }

    onSelect = (e: any) => {
        var canBeChair = true;
        if (e.target.checked) {
            var reviewerInPanel = this.state.reviewersInPanel.find((el: ReviewerInPanelStruct) => el.isChair);
            // can only have one chair
            canBeChair = reviewerInPanel === undefined;
        }
        if (canBeChair) {
            var index = e.target.id;
            var reviewersInPanel: [ReviewerInPanelStruct] = this.state.reviewersInPanel;
            reviewersInPanel[index].isChair = e.target.checked;
            this.setState({ ...this.state, reviewersInPanel: reviewersInPanel });
        } else {
            alert("A panel can only have one chair")
        }
    }

    render() {
        const { reviewersInPanel, reviewers } = this.state;
        var title = this.props.title + " - Panel";
        return (
            <div className="p-5">
                <h1 className="mb-5">{title}</h1>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Reviewer</th>
                            <th>IsChair</th>
                            <th></th>
                        </tr>
                    </thead>
                    {reviewersInPanel.map((reviewerInPanel: ReviewerInPanelStruct, index: number) => (
                        <tbody key={index}>
                            <tr>
                                {reviewerInPanel.id !== 0 &&
                                    this.getReviewerNameById(reviewerInPanel.reviewerId)
                                }
                                {reviewerInPanel.id === 0 &&
                                    <td>
                                        <Form.Control key={index} name="reviewerId" as="select" defaultValue={reviewersInPanel.reviewerId}
                                            onChange={this.onChange}>
                                            {reviewers.map((reviewer: ReviewerStruct, i: number) => (
                                                <option key={i} value={reviewer.id}>{reviewer.name}</option>
                                            ))}
                                        </Form.Control>
                                    </td>
                                }
                                <td>
                                    <Form.Check id={"" + index} type="checkbox" checked={reviewerInPanel.isChair} onChange={this.onSelect} />
                                </td>
                                <td>
                                    <button tabIndex={index} className="link-button"
                                        onClick={this.deleteReviewerInPanel(reviewerInPanel.id, index, reviewerInPanel.isLocal!)}><IoTrashOutline />
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    ))}
                </Table>
                <Row className="mt-5">
                    <Col xs={2}>
                        <Button variant="primary" size="lg" block onClick={this.savePanel}>
                            Save
                        </Button>
                    </Col>
                    <Col xs={3}>
                        <Button variant="secondary" size="lg" block onClick={this.addReviewerInPanel}>
                            Associate Reviewer to Panel
                        </Button>
                    </Col>

                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.job_reducer.callId,
    title: state.job_reducer.title
});

export default connect(mapStateToProps, {})(PanelDetails);