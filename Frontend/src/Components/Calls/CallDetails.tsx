import React, { Component } from 'react';
import axios from 'axios';
import { Form, Col, Button, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { addDataInApp } from '../Common/Utils/Redux/Actions/JobAction';
import { DataInAppStruct, DataInCallStruct } from '../Common/Utils/StructTypes';
import { PathsLabel, ServicePathsLabel, SubServicesLable, Roles } from '../Common/Utils/Paths';

class CallDetails extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            id: 0,
            title: '',
            sponsorId: 0,
            sponsor: '',
            description: '',
            requirements: '',
            funding: 0,
            openingDate: '',
            closingDate: '',
            dataItems: []
        }
        this.handleSaveCall = this.handleSaveCall.bind(this);
        this.appDetails = this.appDetails.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        // mudar o service para retornar o nome do Sponsor
        var callId = this.props.id;
        if (callId !== null && callId !== 0) {
            axios.get(ServicePathsLabel.Calls + callId)
                .then(response => {
                    const call = response.data;
                    axios.get(ServicePathsLabel.Sponsors + call.sponsorId)
                        .then(response => {
                            this.setState({ sponsor: response.data.name });
                        })
                        .catch(error => {
                            console.log("Error:" + error);
                        })
                    this.setState({
                        id: call.id,
                        title: call.title,
                        sponsorId: call.sponsorId,
                        description: call.description,
                        requirements: call.requirements,
                        funding: call.funding,
                        openingDate: call.openingDate,
                        closingDate: call.closingDate
                    });
                })
                .catch(error => {
                    console.log("Error:" + error);
                })

            axios.get(ServicePathsLabel.Calls + SubServicesLable.CallData + callId)
                .then(response => {
                    this.setState({ dataItems: response.data });
                })
                .catch(error => {
                    console.log("Error:" + error);
                })
        }
    }

    async handleSaveCall() {
        let json = {
            id: this.state.id,
            title: this.state.title,
            sponsorId: sessionStorage.getItem('userId'),
            description: this.state.description,
            requirements: this.state.requirements,
            funding: this.state.funding,
            openingDate: new Date(this.state.openingDate),
            closingDate: new Date(this.state.closingDate),
        }
        

        if (this.props.id === null || this.props.id === 0) {
            await axios.post(ServicePathsLabel.Calls, json)
                .then(response => {
                    window.location.hash = PathsLabel.Calls;
                }).catch(error => {
                    console.log("Error: " + error);
                    //sweetAlert error
                });
           
        } else {
            await axios.put(ServicePathsLabel.Calls + this.props.id, json)
                .then(response => {
                    window.location.hash = PathsLabel.Calls;
                }).catch(error => {
                    console.log("Error: " + error);
                    //sweetAlert error
                });
        }

    }

    appDetails() {
        // replicate dataInCall to dataInApp
        const appId = 0;
        var dataInApp: Array<DataInAppStruct> = [];
        this.state.dataItems.forEach((dataItem: DataInCallStruct) => {
            var newData: DataInAppStruct = { id: 0, applicationId: appId, dataItemId: dataItem.dataItemId, data: "" }
            dataInApp.push(newData);
        });
        // saves in store, so it can be added to App after saving
        console.log(dataInApp)
        this.props.addDataInApp(dataInApp);
        window.location.hash = PathsLabel.Applications + PathsLabel.Details;
    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({ ...this.state, [name]: value });
    }

    render() {
        const role = sessionStorage.getItem('role');
        var title = "New Call";
        if (this.state.id !== 0) {
            title = this.state.title;
        }
        return (
            <div className="p-5">
                <Row>
                    <Col>
                        <h1 className="mb-5">{title}</h1>
                    </Col>
                    <Col>
                        {sessionStorage.getItem('role') === Roles.Student && this.state.id !== 0 &&
                            <Button className="float-right mb-3" variant="primary" onClick={this.appDetails}>Create new Application</Button>
                        }
                    </Col>
                </Row>
                <Col xs="5">
                    <Form>
                        <Form.Group controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" name="title" placeholder="Call's title" onChange={this.onChange}
                                value={this.state.title} disabled={role !== Roles.Sponsor} />
                        </Form.Group>
                        <Form.Group controlId="formBasicSponsor">
                            <Form.Label>Sponsor</Form.Label>
                            <Form.Control type="text" name="sponsor" value={this.state.sponsor} disabled />
                        </Form.Group>
                        <Form.Group controlId="formBasicDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" name="description" placeholder="Call's description" onChange={this.onChange}
                                value={this.state.description} disabled={role !== Roles.Sponsor} />
                        </Form.Group>
                        <Form.Group controlId="formBasicRequirement">
                            <Form.Label>Requeriments</Form.Label>
                            <Form.Control type="text" name="requirements" placeholder="Call's requirements" onChange={this.onChange}
                                value={this.state.requirements} disabled={role !== Roles.Sponsor} />
                        </Form.Group>
                        <Form.Group controlId="formBasicFunding">
                            <Form.Label>Funding</Form.Label>
                            <Form.Control type="number" name="funding" placeholder="Call's funding" onChange={this.onChange}
                                value={this.state.funding} disabled={role !== Roles.Sponsor} />
                        </Form.Group>
                        <Form.Group controlId="formBasicOpeningDate" placeholder="Call's opening date">
                            <Form.Label>Opening Date</Form.Label>
                            <Form.Control type="date" name="openingDate" onChange={this.onChange}
                                value={this.state.openingDate} disabled={role !== Roles.Sponsor} />
                        </Form.Group>
                        <Form.Group controlId="formBasicClosingDate">
                            <Form.Label>Closing Date</Form.Label>
                            <Form.Control type="date" name="closingDate" placeholder="Call's closing date" onChange={this.onChange}
                                value={this.state.closingDate} disabled={role !== Roles.Sponsor} />
                        </Form.Group>
                        {role === Roles.Sponsor &&
                            <Row>
                                <Col>
                                    <Button variant="primary" size="lg" onClick={this.handleSaveCall} block>
                                        Save
                                </Button>
                                </Col>
                                <Col>
                                    <Button href={"#" + PathsLabel.Calls} variant="secondary" size="lg" block >
                                        Cancel
                                </Button>
                                </Col>
                            </Row>
                        }
                    </Form>
                </Col>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.job_reducer.callId
    //mentees: getMentees(state)
});

export default connect(mapStateToProps, { addDataInApp })(CallDetails);