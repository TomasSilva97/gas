import React, { Component } from 'react';
import { IoTrashOutline } from "react-icons/io5";
import { FaUsers, FaSearch, FaDatabase } from 'react-icons/fa';
import axios from 'axios';
import { connect } from 'react-redux';
import { addCallId, cleanCallId } from '../Common/Utils/Redux/Actions/JobAction';
import { ServicePathsLabel, PathsLabel, StorageNames, Roles, SubServicesLable } from '../Common/Utils/Paths';
import { CallStruct } from '../Common/Utils/StructTypes';
import { Table, Button, Col, Row } from 'react-bootstrap';


class CallList extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            calls: [],
            oldCalls: [], // used for the search
            openCalls: [], //open calls
            currIndex: 0, //default index of show list
            isLoading: false// meter loading spinner, talvez usar
        }
        // Functions
        this.callDetails = this.callDetails.bind(this);
        this.deleteCall = this.deleteCall.bind(this);
        this.GetPanel = this.GetPanel.bind(this);
        this.onSearch = this.onSearch.bind(this);

    }

    /**Receives the list of calls */
    componentDidMount() {
        axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('auth');
        /*
        if (sessionStorage.getItem('userId') === null)
            window.location.hash = PathsLabel.Login;
        */

        const role = sessionStorage.getItem('role');


        //show only open calls
        if (role === Roles.Student) {
            axios.get(ServicePathsLabel.Calls + SubServicesLable.CallOpen)
                .then(response => {
                    var calls = response.data;
                    this.setState({ calls: calls, oldCalls: calls, isLoading: false });
                })
                .catch(error => {
                    console.log("Error: " + error);
                });
        }
        //show all grant calls
        else {
            axios.get(ServicePathsLabel.Calls)
                .then(response => {
                    var calls = response.data;
                    this.setState({ calls: calls, oldCalls: calls, isLoading: false });
                })
                .catch(error => {
                    console.log("Error: " + error);
                });
        }

    }

    callDetails = (id: number) => (event: React.MouseEvent) => {
        if (id === -1) {
            this.props.cleanCallId(); // removes from the store
        } else {
            this.props.addCallId(id); // adds id to store
        }
        window.location.hash = PathsLabel.Calls + PathsLabel.Details;
        event.preventDefault();
    }

    deleteCall = (id: number) => async (event: React.MouseEvent) => {
        await axios.delete(ServicePathsLabel.Calls + id)
            .then(response => {
                var newCalls = this.state.calls.filter((el: any) => el.id !== id);
                this.setState({ calls: newCalls, oldCalls: newCalls });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        event.preventDefault();
    }

    GetPanel = (id: number, title: string) => (event: React.MouseEvent) => {
        this.props.addCallId(id, title);
        sessionStorage.setItem(StorageNames.activeMenuItem, "Calls");
        window.location.hash = PathsLabel.Panels + PathsLabel.Details;
        event.preventDefault();
    }

    GetDataInCalls = (id: number, title: string) => (event: React.MouseEvent) => {
        this.props.addCallId(id, title);
        sessionStorage.setItem(StorageNames.activeMenuItem, "Calls");
        window.location.hash = PathsLabel.Calls + PathsLabel.DataItems;
        event.preventDefault();
    }

    onSearch(e: any) {
        const target = e.target;
        const value: string = target.value;
        var calls = this.state.oldCalls;
        calls = this.state.oldCalls.filter((call: CallStruct) => {
            return call.title.includes(value)
        });
        this.setState({ calls: calls })
        // guardar em storage o search
    }

    render() {
        const { calls, isLoading } = this.state;
        const role = sessionStorage.getItem('role');
        if (isLoading) {
            return <p>Loading...</p>;//substituir por loading spinner
        }
        return (
            <div className="p-5">
                <h1 className="float-left">List of Calls</h1>
                {sessionStorage.getItem('role') === Roles.Sponsor &&
                    <Button className="float-right mb-3" variant="primary" onClick={this.callDetails(-1)}>Create new Call</Button>
                }
                {/** Search bar */}
                <Row className="input-group mb-3 pt-5">
                    <Col xs={4}>
                        <div className="input-group">
                            <input className="form-control" type="text" placeholder="Search by title..." onChange={this.onSearch} />
                            <div className="input-group-append">
                                <span className="input-group-text" id="basic-addon2"><FaSearch /></span>
                            </div>
                        </div>
                    </Col>
                </Row>
                {calls.length === 0 &&
                    <div className="mt-3 ml-5">
                        <br />
                        <br />
                        <h2><p>There are no Calls to show...</p></h2>
                    </div>
                }
                {calls.length !== 0 &&
                    <div>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Requirements</th>
                                    <th>Funding</th>
                                    <th>Opening date</th>
                                    <th>Closing date</th>
                                    {role === Roles.Sponsor &&
                                        <th></th>
                                    }
                                </tr>
                            </thead>
                            {calls.map((call: CallStruct, i: number) => (
                                <tbody key={i}>
                                    <tr>
                                        <td>
                                            <button className="link-button" onClick={this.callDetails(call.id)}>{call.title}</button>
                                        </td>
                                        <td>{call.description}</td>
                                        <td>{call.requirements}</td>
                                        <td>{call.funding + " €"}</td>
                                        <td>{call.openingDate}</td>
                                        <td>{call.closingDate}</td>
                                        {role === Roles.Sponsor &&
                                            <td>
                                                <button className="link-button" onClick={this.deleteCall(call.id)}><IoTrashOutline /></button>
                                                <button className="ml-3 link-button " onClick={this.GetDataInCalls(call.id, call.title)}><FaDatabase /></button>
                                                <button className="ml-3 link-button " onClick={this.GetPanel(call.id, call.title)}><FaUsers /></button>
                                            </td>
                                        }
                                    </tr>
                                </tbody>
                            ))}
                        </Table>
                    </div>
                }
            </div>
        );
    }

}

export default connect(null, { addCallId, cleanCallId })(CallList);