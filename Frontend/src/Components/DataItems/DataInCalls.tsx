import React, { Component } from 'react';
import { IoTrashOutline } from "react-icons/io5";
import axios from 'axios';
import { connect } from 'react-redux';
import { ServicePathsLabel, SubServicesLable, PathsLabel } from '../Common/Utils/Paths';
import { DataInCallStruct, DataItemStruct } from '../Common/Utils/StructTypes';
import { Table, Form, Row, Col, Button } from 'react-bootstrap';


class DataInCall extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            dataItems: [], //dataInCalls
            datas: [] // dataItems for combo Box
        }
        // Functions
        this.addDataInCall = this.addDataInCall.bind(this);
        this.saveDataInCall = this.saveDataInCall.bind(this);
        this.deleteDataItems = this.deleteDataItems.bind(this);
        this.getDataItemNameById = this.getDataItemNameById.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSelect = this.onSelect.bind(this);
    }

    /**Receives the list of calls */
    componentDidMount() {
        axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('auth');

        var callId = this.props.id;

        axios.get(ServicePathsLabel.Calls + SubServicesLable.CallData + callId)
            .then(response => {
                this.setState({ dataItems: response.data });
            })
            .catch(error => {
                console.log("Error:" + error);
            })

        axios(ServicePathsLabel.DataItems)
            .then(response => {
                this.setState({ datas: response.data });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    addDataInCall() {
        var defaultDataItem = 0;
        if (this.state.datas.length > 0)
            defaultDataItem = this.state.datas[0].id;
        var dataInCall = {
            id: 0,
            callId: this.props.id,
            dataItemId: defaultDataItem,
            isMandatory: false
        }
        var dataItems = this.state.dataItems;
        dataItems.push(dataInCall);
        console.log(dataItems)
        this.setState({ dataItems: dataItems });
    }

    saveDataInCall() {
        var dataItems: Array<DataInCallStruct> = [];

        this.state.dataItems.forEach((el: DataInCallStruct) => {
            var dataItem = { id: el.id, callId: parseInt(this.props.id, 10), dataItemId: el.dataItemId, isMandatory: el.isMandatory };
            dataItems.push(dataItem);
        })
        console.log(dataItems)

        axios.post(ServicePathsLabel.Calls + SubServicesLable.CallData, dataItems)
            .then(response => {
                window.location.hash = PathsLabel.Calls;
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    deleteDataItems = (id: number) => async (event: React.MouseEvent) => {
        await axios.delete(ServicePathsLabel.Calls + SubServicesLable.CallData + id)
            .then(response => {
                var newDataItems = this.state.dataItems.filter((el: any) => el.id !== id);
                this.setState({ dataItems: newDataItems, oldDataItems: newDataItems });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        event.preventDefault();
    }

    getDataItemNameById(id: number) {
        console.log(id)
        console.log(this.state.dataItems)
        var data = this.state.datas.find((el: DataItemStruct) => el.id === id);
        var name = "";
        if (data) {
            name = data.name;

            return (
                <td>
                    {name}
                </td>
            );
        }
    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        var index = e.target.options.selectedIndex;

        var dataItems: [DataInCallStruct] = this.state.reviewersInPanel;
        dataItems[index].dataItemId = parseInt(value, 10);

        this.setState({ ...this.state, dataItems: dataItems });
    }

    onSelect = (e: any) => {
        var index = e.target.id;
        var dataItems: [DataInCallStruct] = this.state.dataItems;
        dataItems[index].isMandatory = e.target.checked;
        this.setState({ ...this.state, dataItems: dataItems });
    }

    render() {
        const { datas, dataItems, isLoading } = this.state;
        var title = this.props.title + " - DataItems";
        if (isLoading) {
            return <p>Loading...</p>;//substituir por loading spinner
        }
        return (
            <div className="p-5">
                <div>
                    <h1 className="float-left">{title}</h1>
                </div>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Data Item Name</th>
                            <th>Mandatory</th>
                            <th></th>
                        </tr>
                    </thead>

                    {dataItems.map((dataItem: DataInCallStruct, i: number) => (
                        <tbody key={i}>
                            <tr>
                                {dataItem.id !== 0 &&
                                    this.getDataItemNameById(dataItem.dataItemId)
                                }
                                {dataItem.id === 0 &&
                                    <td>
                                        <Form.Control key={i} name="dataItemId" as="select" defaultValue={dataItem.dataItemId}
                                            onChange={this.onChange}>
                                            {datas.map((data: DataItemStruct, i: number) => (
                                                <option key={i} value={data.id}>{data.name}</option>
                                            ))}
                                        </Form.Control>
                                    </td>
                                }
                                <td>
                                    <Form.Check id={"" + i} type="checkbox" checked={dataItem.isMandatory} onChange={this.onSelect} />
                                </td>
                                <td>
                                    <button className="link-button" onClick={this.deleteDataItems(dataItem.id)}><IoTrashOutline /></button>
                                </td>
                            </tr>
                        </tbody>
                    ))}
                </Table>
                <Row className="mt-5">
                    <Col xs={2}>
                        <Button variant="primary" size="lg" block onClick={this.saveDataInCall}>
                            Save
                        </Button>
                    </Col>
                    <Col xs={3}>
                        <Button variant="secondary" size="lg" block onClick={this.addDataInCall}>
                            Associate Data in Call
                        </Button>
                    </Col>

                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.job_reducer.callId,
    title: state.job_reducer.title
});

export default connect(mapStateToProps, {})(DataInCall);