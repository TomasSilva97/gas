import React, { Component } from 'react';
import { IoTrashOutline } from "react-icons/io5";
import { FaSearch } from 'react-icons/fa';
import axios from 'axios';
import { connect } from 'react-redux';
import { addDataItemId, cleanDataItemId } from '../Common/Utils/Redux/Actions/JobAction';
import { ServicePathsLabel, PathsLabel } from '../Common/Utils/Paths';
import { DataItemStruct } from '../Common/Utils/StructTypes';
import { Table, Button, Row, Col } from 'react-bootstrap';


class DataItemsList extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            dataItems: [],
            oldDataItems: [],
            isLoading: false// meter loading spinner, talvez usar
        }
        // Functions
        this.dataItemDetails = this.dataItemDetails.bind(this);
        this.deleteDataItems = this.deleteDataItems.bind(this);
    }

    /**Receives the list of calls */
    componentDidMount() {
        axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('auth');
        axios(ServicePathsLabel.DataItems)
            .then(response => {
                this.setState({ dataItems: response.data, oldDataItems: response.data, isLoading: false });
                console.log(response.data)
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    dataItemDetails = (id: number) => (event: React.MouseEvent) => {
        if (id === -1) {
            this.props.cleanDataItemId(); // removes from the store
        } else {
            this.props.addDataItemId(id); // adds id to store
        }
        window.location.hash = PathsLabel.DataItems + PathsLabel.Details;
        event.preventDefault();
    }

    deleteDataItems = (id: number) => async (event: React.MouseEvent) => {
        await axios.delete(ServicePathsLabel.DataItems + id)
            .then(response => {
                var newDataItems = this.state.dataItems.filter((el: any) => el.id !== id);
                this.setState({ dataItems: newDataItems, oldDataItems: newDataItems });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        event.preventDefault();
    }

    onSearch(e: any) {
        const target = e.target;
        const value: string = target.value;
        var dataItems = this.state.oldDataItems;
        dataItems = this.state.oldDataItems.filter((dataItem: DataItemStruct) => {
            return dataItem.name.includes(value)
        });
        this.setState({ dataItems: dataItems })
        // guardar em storage o search
    }

    render() {
        const { dataItems, isLoading } = this.state;
        if (isLoading) {
            return <p>Loading...</p>;//substituir por loading spinner
        }
        return (
            <div className="p-5">
                <div>
                    <h1 className="float-left">List of DataItems
                    </h1>
                    <Button className="float-right mb-3" variant="primary" onClick={this.dataItemDetails(-1)}>Create new Data Item</Button>
                    {/** Search bar */}
                    <Row className="input-group mb-3 pt-5">
                        <Col xs={4}>
                            <div className="input-group">
                                <input className="form-control" type="text" placeholder="Search by name..." onChange={this.onSearch} />
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2"><FaSearch /></span>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
                {dataItems.length === 0 &&
                    <div className="mt-3 ml-5">
                        <br />
                        <br />
                        <h2><p>There are no Data Items to show...</p></h2>
                    </div>
                }
                {dataItems.length !== 0 &&
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th></th>
                            </tr>
                        </thead>
                        {dataItems.map((dataItem: DataItemStruct, i: number) => (
                            <tbody key={i}>
                                <tr>
                                    <td><button className="link-button" onClick={this.dataItemDetails(dataItem.id)}>{dataItem.name}</button></td>
                                    <td>{dataItem.dataType}</td>
                                    <td>
                                        <button className="link-button" onClick={this.deleteDataItems(dataItem.id)}><IoTrashOutline /></button>
                                    </td>
                                </tr>
                            </tbody>
                        ))}
                    </Table>
                }
            </div>
        );
    }

}

export default connect(null, { addDataItemId, cleanDataItemId })(DataItemsList);