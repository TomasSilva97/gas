import React, { Component } from 'react';
import { FaSearch, FaCheckCircle, FaTimesCircle } from 'react-icons/fa';
import axios from 'axios';
import { connect } from 'react-redux';
import { addStudentId, cleanStudentId } from '../Common/Utils/Redux/Actions/JobAction';
import { ServicePathsLabel, PathsLabel } from '../Common/Utils/Paths';
import { StudentStruct } from '../Common/Utils/StructTypes';
import { Table, Col, Row } from 'react-bootstrap';


class StudentsList extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            students: [],
            oldStudents: [], // used for the search
            isLoading: false// meter loading spinner, talvez usar
        }
        // Functions
        this.studentDetails = this.studentDetails.bind(this);
        this.activateStudent = this.activateStudent.bind(this);
        this.deleteStudent = this.deleteStudent.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    /**Receives the list of calls */
    componentDidMount() {
        axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('auth');

        if (sessionStorage.getItem('userId') === null)
            window.location.hash = PathsLabel.Login;

        axios.get(ServicePathsLabel.Students)
            .then(response => {
                this.setState({ students: response.data, oldStudents: response.data, isLoading: false });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    studentDetails = (id: number) => (event: React.MouseEvent) => {
        if (id === -1) {
            this.props.cleanStudentId(); // removes from the store
        } else {
            this.props.addStudentId(id); // adds id to store
        }
        window.location.hash = PathsLabel.Students + PathsLabel.Details;
        event.preventDefault();
    }

    activateStudent = (id: number) => (event: React.MouseEvent) => {
        axios.put(ServicePathsLabel.Students + "activate/" + id)
            .then(response => {
                var newStudents = this.state.students.filter((el: any) => el.id !== id);
                this.setState({ students: newStudents, oldStudents: newStudents });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        event.preventDefault();
    }

    deleteStudent = (id: number) => (event: React.MouseEvent) => {
        axios.delete(ServicePathsLabel.Students + id)
            .then(response => {
                var newStudents = this.state.students.filter((el: any) => el.id !== id);
                this.setState({ students: newStudents, oldStudents: newStudents });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        event.preventDefault();
    }

    onSearch(e: any) {
        const target = e.target;
        const value: string = target.value;
        var students = this.state.oldStudents;
        students = this.state.oldStudents.filter((student: StudentStruct) => {
            return student.name.includes(value)
        });
        this.setState({ students: students })
        // guardar em storage o search
    }

    render() {
        const { students, isLoading } = this.state;
        if (isLoading) {
            return <p>Loading...</p>;//substituir por loading spinner
        }
        return (
            <div className="p-5">
                <h1 className="float-left">List of Students</h1>
                {/** Search bar */}
                <Row className="input-group mb-3 pt-5">
                    <Col xs={4}>
                        <div className="input-group">
                            <input className="form-control" type="text" placeholder="Search by name..." onChange={this.onSearch} />
                            <div className="input-group-append">
                                <span className="input-group-text" id="basic-addon2"><FaSearch /></span>
                            </div>
                        </div>
                    </Col>
                </Row>

                {students.length === 0 &&
                    <div className="mt-3 ml-5">
                        <br />
                        <br />
                        <h2><p>There are no Students to show...</p></h2>
                    </div>
                }
                {students.length !== 0 &&
                    <div>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th></th>
                                </tr>
                            </thead>
                            {students.map((student: StudentStruct, i: number) => (
                                <tbody key={i}>
                                    <tr>
                                        <td><button className="link-button" onClick={this.studentDetails(student.id)}>{student.name}</button></td>
                                        <td>{student.email}</td>
                                        <td>{student.address}</td>
                                        <td>
                                            <Row>
                                                <Col xs={2}>
                                                    <button className="link-button" onClick={this.activateStudent(student.id)}>< FaCheckCircle /></button>
                                                </Col>
                                                <Col xs={2}>
                                                    <button className="link-button" onClick={this.deleteStudent(student.id)}><FaTimesCircle /></button>
                                                </Col>
                                            </Row>
                                        </td>
                                    </tr>
                                </tbody>
                            ))}
                        </Table>
                    </div>
                }
            </div>
        );
    }

}

export default connect(null, { addStudentId, cleanStudentId })(StudentsList);