import {
    ADD_CALL_ID, CLEAN_CALL_ID, ADD_DATA_ITEM_ID, CLEAN_DATA_ITEM_ID, ADD_REVIEWER_ID,
    CLEAN_REVIEWER_ID, ADD_APP_ID, CLEAN_APP_ID, ADD_STUDENT_ID, CLEAN_STUDENT_ID, ADD_INSTITUTION_ID,
    CLEAN_INSTITUTION_ID, ADD_DATA_IN_APP
} from "../Actions/Types";

const initialState = {
    callId: null,
    title: '',
    dataItemId: null,
    reviewerId: null,
    appId: null,
    studentId: null,
    institutionId: null,
    dataInApp: []
};

export default function (state = initialState, action: any) {
    switch (action.type) {
        case ADD_CALL_ID: return {
            ...state, callId: action.callId, title: action.title
        }
        case CLEAN_CALL_ID: return {
            ...state, callId: null, title: ''
        }
        case ADD_DATA_ITEM_ID: return {
            ...state, dataItemId: action.dataItemId
        }
        case CLEAN_DATA_ITEM_ID: return {
            ...state, dataItemId: null
        }
        case ADD_REVIEWER_ID: return {
            ...state, reviewerId: action.reviewerId
        }
        case CLEAN_REVIEWER_ID: return {
            ...state, reviewerId: null
        }
        case ADD_APP_ID: return {
            ...state, appId: action.appId
        }
        case CLEAN_APP_ID: return {
            ...state, appId: null
        }
        case ADD_STUDENT_ID: return {
            ...state, studentId: action.studentId
        }
        case CLEAN_STUDENT_ID: return {
            ...state, studentId: null
        }
        case ADD_INSTITUTION_ID: return {
            ...state, institutionId: action.institutionId
        }
        case CLEAN_INSTITUTION_ID: return {
            ...state, institutionId: null
        }
        case ADD_DATA_IN_APP: return {
            ...state, dataInApp: action.dataInApp
        }
        default:
            return state;
    }
}