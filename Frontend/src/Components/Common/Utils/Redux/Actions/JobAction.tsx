import {
    ADD_CALL_ID, CLEAN_CALL_ID, ADD_DATA_ITEM_ID, CLEAN_DATA_ITEM_ID,
    ADD_REVIEWER_ID, CLEAN_REVIEWER_ID, ADD_APP_ID, CLEAN_APP_ID, ADD_STUDENT_ID, CLEAN_STUDENT_ID,
    ADD_INSTITUTION_ID, CLEAN_INSTITUTION_ID, ADD_DATA_IN_APP
} from "./Types";

export const addCallId = (callId: number, title: string) => {
    return {
        type: ADD_CALL_ID,
        callId: callId,
        title: title
    };
};

export const cleanCallId = () => {
    return {
        type: CLEAN_CALL_ID
    }
}

export const addDataItemId = (dataItemId: number) => {
    return {
        type: ADD_DATA_ITEM_ID,
        dataItemId: dataItemId
    }
}

export const cleanDataItemId = () => {
    return {
        type: CLEAN_DATA_ITEM_ID
    }
}

export const addReviewerId = (reviewerId: number) => {
    return {
        type: ADD_REVIEWER_ID,
        reviewerId: reviewerId
    }
}

export const cleanReviewerId = () => {
    return {
        type: CLEAN_REVIEWER_ID
    }
}

export const addAppId = (appId: number) => {
    return {
        type: ADD_APP_ID,
        appId: appId
    }
}

export const cleanAppId = () => {
    return {
        type: CLEAN_APP_ID
    }
}

export const addStudentId = (studentId: number) => {
    return {
        type: ADD_STUDENT_ID,
        studentId: studentId
    }
}

export const cleanStudentId = () => {
    return {
        type: CLEAN_STUDENT_ID
    }
}

export const addInstitutionId = (institutionId: number) => {
    return {
        type: ADD_INSTITUTION_ID,
        institutionId: institutionId
    }
}

export const cleanInstitutionId = () => {
    return {
        type: CLEAN_INSTITUTION_ID
    }
}

export const addDataInApp = (dataInApp: []) => {
    return {
        type: ADD_DATA_IN_APP,
        dataInApp: dataInApp
    }
}