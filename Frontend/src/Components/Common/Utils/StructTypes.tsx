/* Used for Register */
export type UserStruct = {
    name: string,
    email: string,
    address: string,
    institutionId: number,
    password: string,
    confirmation: string
}

export type CallStruct = {
    id: number,
    title: string,
    sponsorId: number
    sponsor: string,
    description: string,
    requirements: string,
    funding: number,
    openingDate: string,
    closingDate: string,
    dataItems: [],
    // not sure if necessary
    createdBy?: number,
    createdAt?: string,
    updatedBy?: number,
    updatedAt?: string
}

export type DataItemStruct = {
    id: number,
    name: string,
    dataType: string
}

export type PanelStruct = {
    id: number,
    title: string
}

export type ReviewerInPanelStruct = {
    id: number,
    panelId: number,
    reviewerId: number,
    isChair: boolean,
    isLocal?: boolean // used to check if already submitted to server or if is still local
}

export type ReviewerStruct = {
    id: number,
    institutionId: number,
    institutionName: string,
    institutions: [],
    name: string,
    email: string,
    password: string
}

export type ApplicationStruct = {
    id: number,
    studentid: number,
    callId: number,
    name: string,
    wasAccepted: boolean,
    reviews: []
}

export type StudentStruct = {
    id: number,
    institutionId: number,
    name: string,
    email: string,
    address: string,
    isActive: boolean
}

export type InstitutionStruct = {
    id: number,
    name: string,
    contact: number

}

export type ReviewStruct = {
    id: number,
    applicationId: number,
    reviewerId: number,
    reviewerName: string,
    review: string
}

export type DataInCallStruct = {
    id: number,
    callId: number,
    dataItemId: number,
    isMandatory: boolean
}

export type DataInAppStruct = {
    id: number,
    applicationId: number,
    dataItemId: number,
    data: string // binary???
}