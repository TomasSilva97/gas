import React, { Component } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { StorageNames, ServicePathsLabel, PathsLabel, Roles } from '../Utils/Paths';

const ROLE_STUDENT = "ROLE_STUDENT";
const ROLE_SPONSOR = "ROLE_SPONSOR";
const ROLE_REVIEWER = "ROLE_REVIEWER";

export default class Login extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
        this.handleLogin = this.handleLogin.bind(this);
        this.onChange = this.onChange.bind(this);
        this.redirectUser = this.redirectUser.bind(this);
    }

    async handleLogin() {
        let json = {
            username: this.state.username,
            password: this.state.password
        }

        fetch(ServicePathsLabel.Login, {
            method: "POST",
            body: JSON.stringify(json)
        }).then(response => {
            let auth = response.headers.get("Authorization");
            if (auth != null) {
                sessionStorage.setItem('auth', auth);
                var userId = 0;
                response.json().then(json => {
                    // only for students
                    if (json.active || json.active === undefined) {
                        var roles = json.roles;
                        userId = json.id;
                        sessionStorage.setItem("userId", userId.toString());
                        // function that redirects the user depending of the role
                        this.redirectUser(roles);
                    } else {
                        // user not active
                    }
                })
            }
        }).catch(error => {
            alert("username or password is incorrect!")
            console.log("Error:" + error)
        })

    }

    /* Adds common header Authorization to all services
        Redirect the user depending of his role */
    redirectUser(roles: any) {
        if (roles.includes(ROLE_SPONSOR)) {
            sessionStorage.setItem('role', Roles.Sponsor);
        } else if (roles.includes(ROLE_REVIEWER)) {
            sessionStorage.setItem('role', Roles.Reviewer);
        } else if (roles.includes(ROLE_STUDENT)) {
            sessionStorage.setItem('role', Roles.Student);
        }
        sessionStorage.setItem(StorageNames.activeMenuItem, "Calls");
        window.location.hash = PathsLabel.Calls;
        window.location.reload();
    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({ ...this.state, [name]: value });
    }

    render() {
        return (
            <div className="p-5 w-25 mx-auto">
                <Card>
                    <Card.Header className="h2">Log in to your account</Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Username</Form.Label>
                                <Form.Control type="text" name="username" placeholder="Username or email" onChange={this.onChange} value={this.state.username} />
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" name="password" placeholder="Password" onChange={this.onChange} value={this.state.password} />
                            </Form.Group>
                            <Form.Row>
                                <Form.Group className="ml-2" controlId="formBasicCheckbox">
                                    <Form.Check type="checkbox" label="Remember me" />
                                </Form.Group>
                                <Form.Group className="float-right ml-auto mr-1">
                                    <a href={"#" + PathsLabel.ForgotPassword}>Forgot password?</a>
                                </Form.Group>
                            </Form.Row>
                            <Button variant="primary" size="lg" onClick={this.handleLogin} block>
                                Sign In
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
