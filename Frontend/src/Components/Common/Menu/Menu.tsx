import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { MenuItem } from './MenuItem';
import { StorageNames, PathsLabel, Roles } from '../Utils/Paths';

export default class Menu extends Component {

    constructor(props: any) {
        super(props)
        this.state = {
        }
        this.logout = this.logout.bind(this);

    }

    /**Verifies if the menuItem is the active one */
    getActiveMenuItem(id: string) {
        var className = "";
        if (sessionStorage.getItem(StorageNames.activeMenuItem) === id)
            className = "active";
        return className;
    }

    setActiveMenuItem(id: string): any {
        sessionStorage.setItem(StorageNames.activeMenuItem, id);
        this.setState({});
    }

    logout() {
        sessionStorage.clear();
        this.setState({}); // just to reload the component
        window.location.hash = PathsLabel.Login;
    }

    render() {
        const isLogged = sessionStorage.getItem('userId') !== null;
        const role = sessionStorage.getItem('role');
        return (
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="pl-4">
                        {!isLogged &&
                            <MenuItem id="Login" href={PathsLabel.Login} label="Login" isActive={this.getActiveMenuItem("Login")}
                                onClick={() => this.setActiveMenuItem("Login")}></MenuItem>
                        }
                        {!isLogged &&
                            <MenuItem id="Register" href={PathsLabel.Register} label="Register" isActive={this.getActiveMenuItem("Register")}
                                onClick={() => this.setActiveMenuItem("Register")}></MenuItem>
                        }
                        <MenuItem id="Calls" href={PathsLabel.Calls} label="Calls" isActive={this.getActiveMenuItem("Calls")}
                            onClick={() => this.setActiveMenuItem("Calls")}></MenuItem>

                        {isLogged && role === Roles.Sponsor &&
                            <MenuItem id="DataItems" href={PathsLabel.DataItems} label="DataItems" isActive={this.getActiveMenuItem("DataItems")}
                                onClick={() => this.setActiveMenuItem("DataItems")}></MenuItem>
                        }
                        {isLogged && (role === Roles.Student || role === Roles.Reviewer) &&
                            <MenuItem id="Applications" href={PathsLabel.Applications} label="Applications" isActive={this.getActiveMenuItem("Applications")}
                                onClick={() => this.setActiveMenuItem("Applications")}></MenuItem>
                        }
                        {isLogged && role === Roles.Sponsor &&
                            <MenuItem id="DataItems" href={PathsLabel.Reviewer} label="Reviewers" isActive={this.getActiveMenuItem("Reviewers")}
                                onClick={() => this.setActiveMenuItem("Reviewers")}></MenuItem>
                        }
                        {isLogged && role === Roles.Sponsor &&
                            <MenuItem id="Students" href={PathsLabel.Students} label="Students" isActive={this.getActiveMenuItem("Students")}
                                onClick={() => this.setActiveMenuItem("Students")}></MenuItem>
                        }
                        {isLogged && role === Roles.Sponsor &&
                            <MenuItem id="Institutions" href={PathsLabel.Institutions} label="Institutions" isActive={this.getActiveMenuItem("Institutions")}
                                onClick={() => this.setActiveMenuItem("Institutions")}></MenuItem>
                        }
                    </Nav>
                    {isLogged &&
                        <Nav className="ml-auto mr-4">
                            <MenuItem id="Logout" href="" label="Logout" onClick={this.logout}></MenuItem>
                        </Nav>
                    }
                </Navbar.Collapse>
            </Navbar>
        );
    }

}