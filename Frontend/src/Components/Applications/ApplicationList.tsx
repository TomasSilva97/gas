import React, { Component } from 'react';
import { IoTrashOutline } from "react-icons/io5";
import { FaSearch, FaCheck, FaTimes } from 'react-icons/fa';
import axios from 'axios';
import { connect } from 'react-redux';
import { addAppId, cleanAppId, cleanCallId } from '../Common/Utils/Redux/Actions/JobAction';
import { ServicePathsLabel, PathsLabel, Roles } from '../Common/Utils/Paths';
import { ApplicationStruct } from '../Common/Utils/StructTypes';
import { Table, Col, Row, Tabs, Tab } from 'react-bootstrap';


class ApplicationList extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            apps: [],
            oldApps: [],
            currSubApps: [], // current submissions
            evalSubApps: [], // evaluated submissions
            currIndex: 0, // default index of tab
            isLoading: false // meter loading spinner, talvez usar
        }
        // Functions
        this.appDetails = this.appDetails.bind(this);
        this.deleteApp = this.deleteApp.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        //Private methods
        this.getCurrSubmission = this.getCurrSubmission.bind(this);
        this.getEvaluatedSubmission = this.getEvaluatedSubmission.bind(this);
    }

    /**Receives the list of applications */
    componentDidMount() {
        const role = sessionStorage.getItem('role');

        if(role === Roles.Reviewer || role === Roles.Student ){
        axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('auth');
        
        this.props.cleanCallId();
        
        const userId = sessionStorage.getItem('userId');

        var api = ServicePathsLabel.Applications + "";
        if (role === Roles.Student)
            api += ServicePathsLabel.Students + userId;
        else if (role === Roles.Reviewer)
            api += ServicePathsLabel.Reviewers + userId;

        axios(api)
            .then(response => {
                this.setState({ apps: response.data, oldApps: response.data, isLoading: false });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        if (role === Roles.Student) {
            this.getCurrSubmission();
            this.getEvaluatedSubmission();
        }
    }
        else
        window.location.hash = PathsLabel.Calls ;
    }

    appDetails = (id: number) => (event: React.MouseEvent) => {
        console.log(id)
        if (id === -1) {
            this.props.cleanAppId(); // removes from the store
        } else {
            this.props.addAppId(id); // adds id to store
        }
        window.location.hash = PathsLabel.Applications + PathsLabel.Details;
        event.preventDefault();
    }

    deleteApp = (id: number) => (event: React.MouseEvent) => {
        axios.delete(ServicePathsLabel.Applications + id)
            .then(response => {
                var newApps = this.state.apps.filter((el: any) => el.id !== id);
                this.setState({ apps: newApps, oldApps: newApps });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        event.preventDefault();
    }

    onSearch(e: any) {
        const target = e.target;
        const value: string = target.value;
        var apps = this.state.oldApps;
        apps = this.state.oldApps.filter((app: ApplicationStruct) => {
            return app.name.includes(value)
        });
        this.setState({ apps: apps })
        // guardar em storage o search
    }

    onTabChange(index: any) {
        console.log(index)
        const currIndex = this.state.currIndex;
        if (index === 0 && currIndex !== index) {
            const apps = this.state.apps;
            this.setState({ oldApps: apps });
        } else if (index === 2 && currIndex !== index) {
            const currSubApps = this.state.currSubApps;
            this.setState({ oldApps: currSubApps });
        } else if (index === 3 && currIndex !== index) {
            const evalSubApps = this.state.evalSubApps;
            this.setState({ oldApps: evalSubApps });
        }
    }

    //--------------------Private Methods---------------------//

    getCurrSubmission() {
        const userId = sessionStorage.getItem('userId');

        axios(ServicePathsLabel.Applications + ServicePathsLabel.Students + "curr/" + userId)
            .then(response => {
                this.setState({ currSubApps: response.data });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    getEvaluatedSubmission() {
        const userId = sessionStorage.getItem('userId');

        axios(ServicePathsLabel.Applications + ServicePathsLabel.Students + "eval/" + userId)
            .then(response => {
                this.setState({ evalSubApps: response.data });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }


    //--------------------------------------------------------//

    render() {
        const { apps, currSubApps, evalSubApps, isLoading } = this.state;
        const role = sessionStorage.getItem('role');
        if (isLoading) {
            return <p>Loading...</p>;//substituir por loading spinner
        }
        return (
            <div className="p-5">
                <h1 className="float-left">List of Applications</h1>
                {/** Search bar */}
                <Row className="input-group mb-3 pt-5">
                    <Col xs={4}>
                        <div className="input-group">
                            <input className="form-control" type="text" placeholder="Search by name..." onChange={this.onSearch} />
                            <div className="input-group-append">
                                <span className="input-group-text" id="basic-addon2"><FaSearch /></span>
                            </div>
                        </div>
                    </Col>
                </Row>
                {role === Roles.Student &&
                    <Tabs defaultActiveKey="1" id="tab" onSelect={(index) => this.onTabChange(index)}>
                        <Tab eventKey="1" title="My Applications">
                            {apps.length === 0 &&
                                <div className="mt-3 ml-5">
                                    <br />
                                    <br />
                                    <h2><p>There are no Applications to show...</p></h2>
                                </div>
                            }
                            {apps.length !== 0 &&
                                <div>
                                    <Table striped bordered hover>
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Was Accepted</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        {apps.map((app: ApplicationStruct, i: number) => (
                                            <tbody key={i}>
                                                <tr>
                                                    <td><button className="link-button" onClick={this.appDetails(app.id)}>{app.name}</button></td>
                                                    {app.wasAccepted &&
                                                        <td><FaCheck /></td>
                                                    }
                                                    {!app.wasAccepted &&
                                                        <td><FaTimes /></td>
                                                    }
                                                    <td>
                                                        <button className="link-button" onClick={this.deleteApp(app.id)}><IoTrashOutline /></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        ))}
                                    </Table>
                                </div>
                            }
                        </Tab>
                        {/** Not accepted yet */}
                        <Tab eventKey="2" title="Current Submissions">
                            {currSubApps.length === 0 &&
                                <div className="mt-3 ml-5">
                                    <br />
                                    <br />
                                    <h2><p>There are no Applications to show...</p></h2>
                                </div>
                            }
                            {currSubApps.length !== 0 &&
                                <div>
                                    <Table striped bordered hover>
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Was Accepted</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        {currSubApps.map((app: ApplicationStruct, i: number) => (
                                            <tbody key={i}>
                                                <tr>
                                                    <td><button className="link-button" onClick={this.appDetails(app.id)}>{app.name}</button></td>
                                                    {app.wasAccepted &&
                                                        <td><FaCheck /></td>
                                                    }
                                                    {!app.wasAccepted &&
                                                        <td><FaTimes /></td>
                                                    }
                                                    <td>
                                                        <button className="link-button" onClick={this.deleteApp(app.id)}><IoTrashOutline /></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        ))}
                                    </Table>
                                </div>
                            }
                        </Tab>
                        {/** Evaluated submissions */}
                        <Tab eventKey="3" title="Evaluated Submissions">
                            {evalSubApps.length === 0 &&
                                <div className="mt-3 ml-5">
                                    <br />
                                    <br />
                                    <h2><p>There are no Applications to show...</p></h2>
                                </div>
                            }
                            {evalSubApps.length !== 0 &&
                                <div>
                                    <Table striped bordered hover>
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Was Accepted</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        {evalSubApps.map((app: ApplicationStruct, i: number) => (
                                            <tbody key={i}>
                                                <tr>
                                                    <td><button className="link-button" onClick={this.appDetails(app.id)}>{app.name}</button></td>
                                                    {app.wasAccepted &&
                                                        <td><FaCheck /></td>
                                                    }
                                                    {!app.wasAccepted &&
                                                        <td><FaTimes /></td>
                                                    }
                                                    <td>
                                                        <button className="link-button" onClick={this.deleteApp(app.id)}><IoTrashOutline /></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        ))}
                                    </Table>
                                </div>
                            }
                        </Tab>
                    </Tabs>
                }
                {role === Roles.Reviewer &&
                    <div>
                        {apps.length === 0 &&
                            <div className="mt-3 ml-5">
                                <br />
                                <br />
                                <h2><p>There are no Applications to show...</p></h2>
                            </div>
                        }
                        {apps.length !== 0 &&
                            <div>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Was Accepted</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    {apps.map((app: ApplicationStruct, i: number) => (
                                        <tbody key={i}>
                                            <tr>
                                                <td><button className="link-button" onClick={this.appDetails(app.id)}>{app.name}</button></td>
                                                {app.wasAccepted &&
                                                    <td><FaCheck /></td>
                                                }
                                                {!app.wasAccepted &&
                                                    <td><FaTimes /></td>
                                                }
                                                <td>
                                                    <button className="link-button" onClick={this.deleteApp(app.id)}><IoTrashOutline /></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    ))}
                                </Table>
                            </div>
                        }
                    </div>
                }
            </div>
        );
    }
}

export default connect(null, { addAppId, cleanAppId, cleanCallId })(ApplicationList);