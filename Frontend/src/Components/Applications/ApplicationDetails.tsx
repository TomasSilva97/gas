import React, { Component } from 'react';
import axios from 'axios';
import { Table, Form, Col, Button, Row } from 'react-bootstrap';
import { FaCheck, FaTimes } from 'react-icons/fa';
import { connect } from 'react-redux';
import { ReviewStruct, DataInAppStruct } from '../Common/Utils/StructTypes';
import { addAppId } from '../Common/Utils/Redux/Actions/JobAction';
import { PathsLabel, ServicePathsLabel, SubServicesLable, Roles } from '../Common/Utils/Paths';

class ApplicationDetails extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            id: 0,
            name: '',
            studentid: 0,
            callId: 0,
            wasAccepted: false,
            reviews: [],
            dataInApp: [],
            dataInAppsToCreate: []
        }
        this.handleSaveApplication = this.handleSaveApplication.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        var appId = this.props.id;
        console.log(appId)
        if (appId != null) {
            axios.get(ServicePathsLabel.Applications + appId)
                .then(response => {
                    const app = response.data;
                    this.setState({
                        id: app.id,
                        name: app.name,
                        studentid: app.studentId,
                        callId: app.callId,
                        wasAccepted: app.wasAccepted
                    });
                    console.log(app.callId)
                })
                .catch(error => {
                    console.log("Error:" + error);
                })
            axios.get(ServicePathsLabel.Applications + SubServicesLable.ApplicationReview + SubServicesLable.Review + appId)
                .then(response => {
                    this.setState({
                        reviews: response.data
                    });
                })
                .catch(error => {
                    console.log("Error:" + error);
                })

            axios.get(ServicePathsLabel.Applications + SubServicesLable.ApplicationData + appId)
                .then(response => {
                    this.setState({
                        dataItems: response.data
                    });
                })
                .catch(error => {
                    console.log("Error:" + error);
                })
        } else if (this.props.callId === null || this.props.callId === undefined || this.props.callId === 0) {
            // No permission
            alert("No permissions")
        }
    }

    newReview = (id: number) => (event: React.MouseEvent) => {
        this.props.addAppId(id);
        window.location.hash = PathsLabel.Reviews + PathsLabel.Details;
    }

    handleSaveApplication() {
        // mudar o sponsorId por uma session variable
        let json = {
            id: this.state.id,
            studentId: sessionStorage.getItem('userId'),
            callId: this.props.callId,
            name: this.state.name,
            wasAccepted: this.state.wasAccepted,
        }
        var appId = 0;
        axios.post(ServicePathsLabel.Applications, json)
            .then(response => {
                appId = response.data;
                console.log(response.data);

            }).catch(error => {
                console.log("Error: " + error);
                //sweetAlert error
            });
        if (this.props.dataInAppsToCreate.length > 0) {
            var dataInApp: Array<DataInAppStruct> = [];

            this.props.dataInAppsToCreate.forEach((data: DataInAppStruct) => {
                var d: DataInAppStruct = { id: data.id, applicationId: appId, dataItemId: data.dataItemId, data: data.data };
                console.log(d);
                dataInApp.push(d);
            }, () => {
                axios.post(ServicePathsLabel.Applications + "data/", dataInApp)
                    .then(response => {
                        // do nothing
                    })
                    .catch(error => {
                        console.log("Error:" + error);
                    })
            })
        }

        //window.location.hash = PathsLabel.Applications;
    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({ ...this.state, [name]: value });
    }

    render() {
        var title = "New Application";
        if (this.state.id !== 0) {
            title = this.state.name;
        }
        return (
            <div className="p-5">
                <div>
                    <h1 className="mb-5">{title}</h1>
                    <Col xs="5">
                        <Form>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" name="name" placeholder="Application's name" onChange={this.onChange} value={this.state.name}>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="formBasicAccepted">
                                <Form.Label>Accepted</Form.Label>
                                {this.state.wasAccepted &&
                                    <Col>
                                        <FaCheck />
                                    </Col>
                                }
                                {!this.state.wasAccepted &&
                                    <Col>
                                        <FaTimes />
                                    </Col>
                                }
                            </Form.Group>
                            <Row>
                                <Col>
                                    <Button variant="primary" size="lg" onClick={this.handleSaveApplication} block>
                                        Save
                                </Button>
                                </Col>
                                <Col>
                                    <Button href={"#" + PathsLabel.Applications} variant="secondary" size="lg" block >
                                        Cancel
                                </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </div>
                <div>
                    <Row>
                        <Col>
                            {this.state.dataInApp.length === 0 &&
                                <div className="mt-3 ml-5">
                                    <br />
                                    <br />
                                    <h2><p>There are no Data In APplication to show...</p></h2>
                                </div>
                            }
                            {this.state.dataInApp.length !== 0 &&
                                <div>
                                    <Table striped bordered hover>
                                        <thead>
                                            <tr>
                                                <th>Data Item</th>
                                                <th>Data</th>
                                            </tr>
                                        </thead>
                                        {this.state.dataInApp.map((dataInApp: DataInAppStruct, i: number) => (
                                            <tbody key={i}>
                                                <tr>
                                                    <td>{dataInApp.dataItemId}</td>
                                                    <td>{dataInApp.data}</td>
                                                </tr>
                                            </tbody>
                                        ))}
                                    </Table>
                                </div>
                            }

                        </Col>
                        <Col>
                            {this.state.reviews.length === 0 &&
                                <div className="mt-3 ml-5">
                                    <br />
                                    <br />
                                    <h2><p>There are no Reviews to show...</p></h2>
                                    {sessionStorage.getItem('role') === Roles.Reviewer &&

                                        <Button className="float-right mb-3" variant="primary" onClick={this.newReview(this.state.id)}>New Review</Button>
                                    }
                                </div>
                            }
                            {this.state.reviews.length !== 0 &&
                                <div>
                                    <Table striped bordered hover>
                                        <thead>
                                            <tr>
                                                <th>Reviewer Name</th>
                                                <th>Review</th>
                                            </tr>
                                        </thead>
                                        {this.state.reviews.map((rev: ReviewStruct, i: number) => (
                                            <tbody key={i}>
                                                <tr>
                                                    <td>{rev.reviewerId}</td>
                                                    <td>{rev.review}</td>

                                                </tr>
                                            </tbody>
                                        ))}
                                    </Table>
                                </div>
                            }
                        </Col>
                    </Row>
                </div>
            </div>


        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.job_reducer.appId,
    callId: state.job_reducer.callId,
    dataInAppsToCreate: state.job_reducer.dataInApp
    //mentees: getMentees(state)
});

export default connect(mapStateToProps, { addAppId })(ApplicationDetails);

