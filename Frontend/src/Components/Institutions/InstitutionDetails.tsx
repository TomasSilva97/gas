import React, { Component } from 'react';
import axios from 'axios';
import { Form, Col, Button, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { InstitutionStruct } from '../Common/Utils/StructTypes';
import { PathsLabel, ServicePathsLabel } from '../Common/Utils/Paths';

class InstitutionDetails extends Component<InstitutionStruct, InstitutionStruct>{
    constructor(props: InstitutionStruct) {
        super(props)
        this.state = {
            id: 0,
            name: '',
            contact: 0
        }
        this.handleSaveInstitution = this.handleSaveInstitution.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        // mudar o service para retornar o nome do Sponsor
        var institutionId = this.props.id;
        if (institutionId != null) {
            axios.get(ServicePathsLabel.Institutions + institutionId)
                .then(response => {
                    const inst = response.data;
                    this.setState({
                        id: inst.id,
                        name: inst.name,
                        contact: inst.contact
                    });
                })
                .catch(error => {
                    console.log("Error:" + error);
                })
        }
    }

    handleSaveInstitution() {
        // mudar o sponsorId por uma session variable
        let json = {
            id: this.state.id,

            name: this.state.name,
            contact: this.state.contact
        }

        axios.post(ServicePathsLabel.Institutions, json)
            .then(response => {
                window.location.hash = PathsLabel.Institutions;
            }).catch(error => {
                console.log("Error: " + error);
                //sweetAlert error
            });
    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({ ...this.state, [name]: value });
    }

    render() {
        var title = "New Institution";
        if (this.state.id !== 0) {
            title = this.state.name;
        }
        return (
            <div className="p-5">
                <h1 className="mb-5">{title}</h1>
                <Col xs="5">
                    <Form>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" name="name" placeholder="Institutions's name" onChange={this.onChange} value={this.state.name}>
                            </Form.Control>
                            <Form.Label>Contact</Form.Label>
                            <Form.Control type="number" name="contact" placeholder="Institutions's contact" onChange={this.onChange} value={this.state.contact}>
                            </Form.Control>
                        </Form.Group>

                        <Row>
                            <Col>
                                <Button variant="primary" size="lg" onClick={this.handleSaveInstitution} block>
                                    Save
                                </Button>
                            </Col>
                            <Col>
                                <Button href={"#" + PathsLabel.Institutions} variant="secondary" size="lg" block >
                                    Cancel
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.job_reducer.institutionId
    //mentees: getMentees(state)
});

export default connect(mapStateToProps, {})(InstitutionDetails);