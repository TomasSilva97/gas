import React, { Component } from 'react';
import { IoTrashOutline } from "react-icons/io5";
import { FaSearch } from 'react-icons/fa';
import axios from 'axios';
import { connect } from 'react-redux';
import { addInstitutionId, cleanInstitutionId } from '../Common/Utils/Redux/Actions/JobAction';
import { ServicePathsLabel, PathsLabel, Roles } from '../Common/Utils/Paths';
import { InstitutionStruct } from '../Common/Utils/StructTypes';
import { Table, Button, Col, Row } from 'react-bootstrap';


class InstitutionList extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            institutions: [],
            oldInstitutions: [],
            isLoading: false// meter loading spinner, talvez usar
        }
        // Functions
        this.institutionDetails = this.institutionDetails.bind(this);
        this.deleteInstitution = this.deleteInstitution.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    /**Receives the list of institutions */
    componentDidMount() {
        axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('auth');
        axios(ServicePathsLabel.Institutions)
            .then(response => {
                this.setState({ institutions: response.data, oldInstitutions: response.data, isLoading: false });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    institutionDetails = (id: number) => (event: React.MouseEvent) => {
        console.log(id)
        if (id === -1) {
            this.props.cleanInstitutionId(); // removes from the store
        } else {
            this.props.addInstitutionId(id); // adds id to store
        }
        window.location.hash = PathsLabel.Institutions + PathsLabel.Details;
        event.preventDefault();
    }

    deleteInstitution = (id: number) => (event: React.MouseEvent) => {
        axios.delete(ServicePathsLabel.Institutions + id)
            .then(response => {
                var newInst = this.state.institutions.filter((el: any) => el.id !== id);
                this.setState({ institutions: newInst, oldInstitutions: newInst });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
        event.preventDefault();
    }


    onSearch(e: any) {
        const target = e.target;
        const value: string = target.value;
        var insts = this.state.oldInstitutions;
        insts = this.state.oldInstitutions.filter((institutions: InstitutionStruct) => {
            return institutions.name.includes(value)
        });
        this.setState({ institutions: insts })
        // guardar em storage o search
    }

    render() {
        const { institutions, isLoading } = this.state;
        if (isLoading) {
            return <p>Loading...</p>;//substituir por loading spinner
        }
        return (
            <div className="p-5">
                <h1 className="float-left">List of Institutions</h1>
                {sessionStorage.getItem('role') === Roles.Sponsor &&
                    <Button className="float-right mb-3" variant="primary" onClick={this.institutionDetails(-1)}>Create new Institution</Button>
                }
                {/** Search bar */}
                <Row className="input-group mb-3 pt-5">
                    <Col xs={4}>
                        <div className="input-group">
                            <input className="form-control" type="text" placeholder="Search by title..." onChange={this.onSearch} />
                            <div className="input-group-append">
                                <span className="input-group-text" id="basic-addon2"><FaSearch /></span>
                            </div>
                        </div>
                    </Col>
                </Row>
                {institutions.length === 0 &&
                    <div className="mt-3 ml-5">
                        <br />
                        <br />
                        <h2><p>There are no Institutions to show...</p></h2>
                    </div>
                }
                {institutions.length !== 0 &&
                    <div>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Contact</th>
                                </tr>
                            </thead>
                            {institutions.map((inst: InstitutionStruct, i: number) => (
                                <tbody key={i}>
                                    <tr>
                                        <td><button className="link-button" onClick={this.institutionDetails(inst.id)}>{inst.name}</button></td>
                                        <td>{inst.contact}</td>
                                        <td>
                                            <button className="link-button" onClick={this.deleteInstitution(inst.id)}><IoTrashOutline /></button>
                                        </td>
                                    </tr>
                                </tbody>
                            ))}
                        </Table>
                    </div>
                }
            </div>
        );
    }

}

export default connect(null, { addInstitutionId, cleanInstitutionId })(InstitutionList);