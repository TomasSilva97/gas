import React, { Component } from 'react';
import axios from 'axios';
import { Form, Col, Button, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { ReviewStruct } from '../Common/Utils/StructTypes';
import { PathsLabel, ServicePathsLabel, SubServicesLable } from '../Common/Utils/Paths';

class ReviewDetails extends Component<ReviewStruct, ReviewStruct>{
    constructor(props: ReviewStruct) {
        super(props)
        this.state = {
            id: 0,
            applicationId: 0,
            reviewerId: 0,
            reviewerName: "",
            review: ""
        }
        this.handleSaveReview = this.handleSaveReview.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    handleSaveReview() {
        // mudar o sponsorId por uma session variable
        let json = {
            id: this.state.id,
            applicationId: this.props.applicationId,
            reviewerId: Number(sessionStorage.getItem('userId')),
            review: this.state.review
        }

        axios.post(ServicePathsLabel.Applications + SubServicesLable.ApplicationReview, json)
            .then(response => {
                window.location.hash = PathsLabel.Institutions;
            }).catch(error => {
                console.log("Error: " + error);
                //sweetAlert error
            });
    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({ ...this.state, [name]: value });
    }

    render() {
        var title = "New Review";
        return (
            <div className="p-5">
                <h1 className="mb-5">{title}</h1>
                <Col xs="5">
                    <Form>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Review</Form.Label>
                            <Form.Control type="text" name="review" placeholder="Review" onChange={this.onChange} value={this.state.review}>
                            </Form.Control>
                        </Form.Group>

                        <Row>
                            <Col>
                                <Button variant="primary" size="lg" onClick={this.handleSaveReview} block>
                                    Save
                                </Button>
                            </Col>
                            <Col>
                                <Button href={"#" + PathsLabel.Applications} variant="secondary" size="lg" block >
                                    Cancel
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    applicationId: state.job_reducer.appId
    //mentees: getMentees(state)
});

export default connect(mapStateToProps, {})(ReviewDetails);