import React, { Component } from 'react';
import axios from 'axios';
import { Form, Col, Button, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { InstitutionStruct, ReviewerStruct } from '../Common/Utils/StructTypes';
import { PathsLabel, ServicePathsLabel } from '../Common/Utils/Paths';

class ReviewerDetails extends Component<ReviewerStruct, ReviewerStruct>{
    constructor(props: ReviewerStruct) {
        super(props)
        this.state = {
            id: 0,
            institutionId: 0,
            institutionName: '',
            name: '',
            email: '',
            password: '',
            institutions:[]
        }
        this.handleSaveReviewer = this.handleSaveReviewer.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        // mudar o service para retornar o nome do Sponsor
        var reviewerId = this.props.id;
        if (reviewerId !== null && reviewerId !== 0) {
            axios.get(ServicePathsLabel.Reviewers + reviewerId)
                .then(response => {
                    const reviewer = response.data;
                    console.log(response.data)
                    this.setState({
                        id: reviewer.id,
                        institutionId: reviewer.institutionId,
                        institutionName: reviewer.institutionName,
                        name: reviewer.name,
                        email: reviewer.email,
                        password: reviewer.password
                    });
                })
                .catch(error => {
                    console.log("Error:" + error);
                })
        }

        axios(ServicePathsLabel.Institutions)
        .then(response => {
            this.setState({ institutions: response.data});
        })
        .catch(error => {
            console.log("Error: " + error);
        });
    }

    async handleSaveReviewer() {
        // mudar o sponsorId por uma session variable
        let json = {
            id: this.state.id,
            institutionId: this.state.institutionId,
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        }

        if (this.props.id === null || this.props.id === 0) {
            await axios.post(ServicePathsLabel.Reviewers, json)
                .then(response => {
                    window.location.hash = PathsLabel.Reviewer;
                }).catch(error => {
                    console.log("Error: " + error);
                    //sweetAlert error
                });
        } else {
            await axios.put(ServicePathsLabel.Reviewers + this.props.id, json)
                .then(response => {
                    window.location.hash = PathsLabel.Reviewer;
                }).catch(error => {
                    console.log("Error: " + error);
                    //sweetAlert error
                });
        }

    }

    onChange(e: any) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({ ...this.state, [name]: value });
    }

    render() {
        var title = "New Reviewer";
        if (this.state.id !== 0) {
            title = this.state.name;
        }
        return (
            <div className="p-5">
                <h1 className="mb-5">{title}</h1>
                <Col xs="5">
                    <Form>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" name="name" placeholder="Reviewer's name" onChange={this.onChange} value={this.state.name} />
                        </Form.Group>
                        <Form.Group controlId="formBasicInstitution">
                            <Form.Label>Institution</Form.Label>
                            <Form.Control name="institutionId" as="select" defaultValue={this.state.institutionId}
                                onChange={this.onChange}>
                                {this.state.institutions.map((institution: InstitutionStruct, i: number) => (
                                    <option key={i} value={institution.id}>{institution.name}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" name="email" placeholder="Reviewer's email" onChange={this.onChange} value={this.state.email} />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" placeholder="Reviewer's password" onChange={this.onChange} value={this.state.password} />
                        </Form.Group>
                        <Row>
                            <Col>
                                <Button variant="primary" size="lg" onClick={this.handleSaveReviewer} block>
                                    Save
                                </Button>
                            </Col>
                            <Col>
                                <Button href={"#" + PathsLabel.Reviewer} variant="secondary" size="lg" block>
                                    Cancel
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.job_reducer.reviewerId
    //mentees: getMentees(state)
});

export default connect(mapStateToProps, {})(ReviewerDetails);
