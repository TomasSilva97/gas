import React, { Component } from 'react';
import { IoTrashOutline } from "react-icons/io5";
import { FaSearch } from 'react-icons/fa';
import axios from 'axios';
import { connect } from 'react-redux';
import { addReviewerId, cleanReviewerId } from '../Common/Utils/Redux/Actions/JobAction';
import { ServicePathsLabel, PathsLabel } from '../Common/Utils/Paths';
import { ReviewerStruct } from '../Common/Utils/StructTypes';
import { Table, Button, Col, Row } from 'react-bootstrap';


class ReviewersList extends Component<any, any>{
    constructor(props: any) {
        super(props)
        this.state = {
            reviewers: [],
            oldReviewers: [], // used for the search
            isLoading: false// meter loading spinner, talvez usar
        }
        // Functions
        this.reviewerDetails = this.reviewerDetails.bind(this);
        this.deleteReviewer = this.deleteReviewer.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    /**Receives the list of calls */
    componentDidMount() {
        axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('auth');
        axios.get(ServicePathsLabel.Reviewers)
            .then(response => {
                this.setState({ reviewers: response.data, oldReviewers: response.data, isLoading: false });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    reviewerDetails = (id: number) => (event: React.MouseEvent) => {
        if (id === -1) {
            this.props.cleanReviewerId(); // removes from the store
        } else {
            this.props.addReviewerId(id); // adds id to store
        }
        window.location.hash = PathsLabel.Reviewer + PathsLabel.Details;
        event.preventDefault();
    }

    deleteReviewer = (id: number) => async (event: React.MouseEvent) => {
        await axios.delete(ServicePathsLabel.Reviewers + id)
            .then(response => {
                var newReviewers = this.state.reviewers.filter((el: any) => el.id !== id);
                this.setState({ reviewers: newReviewers, oldReviewers: newReviewers });
            })
            .catch(error => {
                console.log("Error: " + error);
            });
            event.persist();
    }

    onSearch(e: any) {
        const target = e.target;
        const value: string = target.value;
        var reviewers = this.state.oldReviewers;
        reviewers = this.state.oldReviewers.filter((reviewer: ReviewerStruct) => {
            return reviewer.name.includes(value)
        });
        this.setState({ reviewers: reviewers })
        // guardar em storage o search
    }

    render() {
        const { reviewers, isLoading } = this.state;
        if (isLoading) {
            return <p>Loading...</p>;//substituir por loading spinner
        }
        return (
            <div className="p-5">
                <h1 className="float-left">List of Reviewers</h1>
                <Button className="float-right mb-3" variant="primary" onClick={this.reviewerDetails(-1)}>Create new Reviewer</Button>
                {/** Search bar */}
                <Row className="input-group mb-3 pt-5">
                    <Col xs={4}>
                        <div className="input-group">
                            <input className="form-control" type="text" placeholder="Search by name..." onChange={this.onSearch} />
                            <div className="input-group-append">
                                <span className="input-group-text" id="basic-addon2"><FaSearch /></span>
                            </div>
                        </div>
                    </Col>
                </Row>

                {reviewers.length === 0 &&
                    <div className="mt-3 ml-5">
                        <br />
                        <br />
                        <h2><p>There are no Reviewers to show...</p></h2>
                    </div>
                }
                {reviewers.length !== 0 &&
                    <div>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th></th>
                                </tr>
                            </thead>
                            {reviewers.map((reviewer: ReviewerStruct, i: number) => (
                                <tbody key={i}>
                                    <tr>
                                        <td><button className="link-button" onClick={this.reviewerDetails(reviewer.id)}>{reviewer.name}</button></td>
                                        <td>{reviewer.email}</td>
                                        <td>
                                            <button className="link-button" onClick={this.deleteReviewer(reviewer.id)}><IoTrashOutline /></button>
                                        </td>
                                    </tr>
                                </tbody>
                            ))}
                        </Table>
                    </div>
                }
            </div>
        );
    }
}

export default connect(null, { addReviewerId, cleanReviewerId })(ReviewersList);