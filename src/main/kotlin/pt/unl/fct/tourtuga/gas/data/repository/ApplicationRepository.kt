package pt.unl.fct.tourtuga.gas.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import pt.unl.fct.tourtuga.gas.data.dao.ApplicationDAO
import pt.unl.fct.tourtuga.gas.data.dao.StudentDAO
import java.time.LocalDate

import java.util.*

interface ApplicationRepository : JpaRepository<ApplicationDAO, Long> {

    fun findAllByStudentId(studentId: Long): List<ApplicationDAO>

    @Query("SELECT a FROM ApplicationDAO  a  JOIN PanelDAO p ON a.callId = p.id" +
            "  JOIN ReviewerInPanelDAO rp ON p.id = rp.panelId  JOIN ReviewerDAO r ON rp.reviewerId = r.id WHERE r.id = :reviewerId")
    fun findAllByReviewerId(reviewerId: Long): List<ApplicationDAO>

    @Query("SELECT s FROM StudentDAO s " +
            "INNER JOIN ApplicationDAO app ON app.studentId = s.id WHERE app.id = :applicationId")
    fun getStudentsInstitutionByApplicationId(applicationId: Long): Optional<StudentDAO>

    @Query("SELECT a FROM ApplicationDAO a INNER JOIN CallDAO c ON c.id = a.callId " +
            "WHERE a.studentId= :id AND :date BETWEEN c.openingDate AND c.closingDate")
    fun findAllSubmitted(id: Long, date: LocalDate) : List<ApplicationDAO>

    @Query("SELECT a FROM ApplicationDAO a INNER JOIN ApplicationReviewDAO ar ON a.id = ar.applicationId " +
    "WHERE a.studentId = :id")
    fun findAllEvaluated(id:Long) : List<ApplicationDAO>

}