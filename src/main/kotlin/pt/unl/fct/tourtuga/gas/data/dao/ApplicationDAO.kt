package pt.unl.fct.tourtuga.gas.data.dao

import pt.unl.fct.tourtuga.gas.api.application.ApplicationDTO
import java.time.LocalDate
import javax.persistence.*

@Entity
class ApplicationDAO(
        @Id @GeneratedValue(strategy = GenerationType.SEQUENCE) var id: Long,
        var studentId: Long,
        var callId: Long,
        var name: String,
        var wasAccepted: Boolean
) {
    constructor(application: ApplicationDTO) : this(application.id, application.studentId, application.callId,
            application.name, application.wasAccepted) {
    }

    //Empty constructor
    constructor() : this(0, 0, 0, "", false)

    fun update(application: ApplicationDTO) {
        this.name = application.name;
        this.callId = application.callId;
        this.wasAccepted = application.wasAccepted;
    }

}